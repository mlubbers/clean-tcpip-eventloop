# Clean select loop

Write network applications in a reactive way in clean

## Libraries

- TCPServer

	Contains the master function `serve`.
- TCPServer.Listener

	Contains an abstraction over `serve` only handling the listening part.
- TCPServer.Connection

	Contains an abstraction over `serve` only handling one tcp connection to a
	server.
- TCPServer.HTTP

	Contains an abstraction over `TCPServer.Connection` that can send one
	`HTTPRequest`.

## Maintainer

Mart Lubbers <mart@cs.ru.nl>

## License

`tcpip-eventloop` is licensed under the BSD 2-Clause "Simplified" License (see [LICENSE](LICENSE)).
